package shuzu;

import java.util.Scanner;

    public class Main1 {
        /*
        十进制-->二进制
        */
        public static void toBin(int num)
        {
            trans(num,1,1);
        }

        /*
        十进制-->八进制
        */
        public static void toOct(int num)
        {
            trans(num,7,3);
        }

        /*
        十进制-->十六进制
        */
        public static void toHex(int num)
        {
            trans(num,15,4);
        }


        public static void trans(int num,int base,int offset)
        {
            char [] chs = { '0','1','2','3',
                    '4','5','6','7',
                    '8','9','A','B',
                    'C','D','E','F'};
            char [] arr = new char[32];
            int pos = arr.length;

            while(num != 0)
            {
                int temp = num & base;
                arr[--pos] = chs[temp];
                num = num >>> offset;
            }

            for(int x = pos;x <arr.length;x++)
                System.out.print(arr[x]);
        }



        public int BinaryToDecimal(int x){

            int j = 0;
            int p = 0;
            while(true){
                if(x == 0){
                    break;
                } else {
                    int temp = x%10;
                    j += temp*Math.pow(2, p);
                    x = x/10;
                    p++;
                }
            }
            return j;
        }


        public static void main(String args[]){
            System.out.println("请输入要转换为十进制的进制数(2、8、16)：");
            Scanner input = new Scanner(System.in);
            int ac = input.nextInt();
            int sum=0;
            if(ac==2||ac==8||ac==16){
                if(ac==2){
                    System.out.println("请输入要转换为十进制的二进制数：");
                    Main1 obj = new Main1();
                    int m = input.nextInt();
                    sum=obj.BinaryToDecimal(m);
                    System.out.println("结果为： "+sum);

                }
                if(ac==8){
                    System.out.println("请输入要转换为十进制的八进制数：");
                    int a[]=new int[1000];
                    int n=input.nextInt();
                    int i=0;
                    while(n>0){
                        a[i]=n%10;
                        n=n/10;
                        sum=(int) (sum+a[i]*Math.pow(8, i));
                        i++;
                    }
                    System.out.println("结果为："+sum);
                }
                if(ac==16){
                    System.out.println("输入十六进制数为：");
                    Scanner scanner=new Scanner(System.in);
                    String string=scanner.next();
                    char[] c=string.toCharArray();
                    int n=c.length;
                    for (int i = 0; i < n; i++){
                        if ((int)c[i]-48>9)
                        {
                            if (c[i]=='A')
                            {
                                //Math.pow(double a, double b)是求a的b次方的
                                sum+=10*Math.pow(16, n-i-1);
                            }
                            if (c[i]=='B') {
                                sum+=11*Math.pow(16, n-i-1);
                            }
                            if (c[i]=='C') {
                                sum+=12*Math.pow(16, n-i-1);
                            }
                            if(c[i]=='D') {
                                sum+=13*Math.pow(16,n-i-1);
                            }
                            if(c[i]=='E') {
                                sum+=14*Math.pow(16,n-i-1);
                            }
                            if(c[i]=='F') {
                                sum+=15*Math.pow(16,n-i-1);
                            }

                        }else {
                            sum+=((int)c[i]-48)*Math.pow(16, n-i-1);
                        }
                    }
                    System.out.println("十进制数为："+sum);
                }

            }
            else{
                System.out.println("无法转换该进制");
                System.exit(0);
            }
            Scanner m = new Scanner(System.in);
            System.out.println("请输入要转化成的进制");
            int jin = m.nextInt();
                int x = sum;
            if(ac==2||ac==8||ac==16) {
                if (jin == 2) {
                    System.out.println(" 十进制-->二进制:");
                    toBin(x);
                }
                if (jin == 8) {
                    System.out.println(" 十进制-->八进制:");
                    toOct(x);
                }
                if (jin == 16) {
                    System.out.println(" 十进制-->十六进制:");
                    toHex(x);
                }
            }
            else {
                    System.out.println("无法转换该进制");
                    System.exit(0);
            }
        }
    }
