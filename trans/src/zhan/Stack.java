package zhan;

//栈
public class Stack {
        private int maxsize;
        private int num[];
        private int top = -1;
        public Stack(int maxsize)
        {
            this.maxsize = maxsize;
            num = new int[maxsize];//为栈申请空间
        }
        //判断栈是否为满
        public boolean isFull()
        {
            return top == maxsize - 1;
        }
        //是否为空
        public boolean isEmpty()
        {
            return top == -1;
        }
        //入栈
        public void push(int temp)
        {
            if(isFull())
            {
                System.out.println("栈已满");
                return;
            }
            num[++top] = temp;
        }
        //出栈
        public int pop()
        {
            if(isEmpty())
            {
                System.out.println("栈已经为空");
                return -1;
            }
            return num[top --];
        }

    }

