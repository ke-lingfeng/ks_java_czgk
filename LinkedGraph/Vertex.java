package LinkedGraph;
public class Vertex<T> {
    T data;
    Arc next;

    public Vertex(T data, Arc next) {
        this.data = data;
        this.next = next;

    }

    public String toString() {
        return data + "";
    }
}