package Graph;

public class Arc {
    public Arc next;
    public int index;// 索引
    int weight;// 权值


    public Arc(int index, int weight, Arc next) {
        this.index = index;
        this.weight = weight;

    }

    public Arc(int index, Arc next) {
        this.index = index;

        this.weight = 0;
    }
}