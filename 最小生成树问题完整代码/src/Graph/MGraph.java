package Graph;
import java.util.Scanner;

public class MGraph implements IGraph {
    private final static int INFINITY =Integer.MAX_VALUE;
    private GraphKind kind;
    public int vexNum,arcNum;//顶点数量，边数量，
    public Object[] vexs;//顶点集合
    public int[][] arcs;//邻接关系集合
    public  MGraph(Object[] vexs, int[][] arcs, int arcNum, int i, GraphKind udg){
        this((GraphKind) null,0,0,null,null);
    }
    public  MGraph(GraphKind kind, int vexNum, int arcNum, Object[] vexs, int[][] arcs){

        this.kind=kind;
        this.vexNum=vexNum;
        this.arcNum=arcNum;
        this.vexs=vexs;
        this.arcs=arcs;
    }
    public MGraph(){
        this(null,0,0,null,null);
    }



    public MGraph(GraphKind graphKind, int i, int i1, Object o, Object o1) {

    }

    @Override
    public void creatGraph() {

        Scanner sc=new Scanner(System.in);
        System.out.println("请输入图的类型：");
        GraphKind kind = GraphKind.valueOf(sc.next());
        switch(kind){
            case UDG:
                createUDG();;
                return ;
            case UDN:
                createUDN();
                return;
        }
    }
    public void createUDG(){

    }
    public void createUDN(){
        Scanner sc=new Scanner(System.in);
        System.out.println("请分别输入图的顶点数、图的边数：：");
        vexNum=sc.nextInt();
        arcNum=sc.nextInt();
        vexs=new Object[vexNum];
        System.out.println("请分别输入图的各个顶点");
        for(int v=0;v<vexNum;v++)
            vexs[v]=sc.next();
        arcs=new int[vexNum][vexNum];
        for(int v=0;v<vexNum;v++)
            for(int u=0;u<vexNum;u++)
                arcs[v][u]=INFINITY;
        System.out.println("请输入各个边的两个顶点及其权值：");
        for(int k=0;k<arcNum;k++){
            int v=locateVex(sc.next());
            int u=locateVex(sc.next());
            arcs[v][u]=arcs[u][v]=sc.nextInt();
        }
    }

    public int getVexNum() {
        return vexNum;
    }

    public int getArcNum() {
        return arcNum;
    }

    public int locateVex(Object vex) {
        for(int v=0;v<vexNum;v++)
            if(vexs[v].equals(vex))
                return v;
        return 0;
    }

    public Object getVex(int v) throws  Exception{
        if(v<0 && v>=vexNum)
            throw new Exception("第"+v+"个顶点不存在！");
        return vexs[v];
    }
    public int firstAdjVex(int v)throws  Exception{
        if(v<0 && v>=vexNum)
            throw new Exception("第"+v+"个顶点不存在！");
        for(int j=0;j<vexNum;j++)
            if(arcs[v][j]!=0 && arcs[v][j]< INFINITY)
                return j;
        return -1;
    }

    public int nextAdjVex(int v,int w)throws  Exception{
        if(v<0 && v>=vexNum)
            throw new Exception("第"+v+"个顶点不存在！");
        for(int j =w+1;j<vexNum;j++)
            return j;
        return -1;
    }
    public GraphKind getKind(){
        return kind;
    }
    public int[][] getArcs(){
        return arcs;

    }
    public Object[] getVexs(){
        return vexs;
    }

}