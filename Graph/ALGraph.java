package Graph;

import java.util.Scanner;

public class ALGraph implements IGraph{
    private GraphKind kind ;
    private int vexNum,arcNum;
    private  VNode[] vexs;

    public ALGraph(){
        this(null,0,0,null);
    }
    public ALGraph(GraphKind kind, int vexNum, int arcNum, VNode[] vexs){
        this.kind=kind;
        this.vexNum=vexNum;
        this.arcNum=arcNum;
        this.vexs=vexs;
    }

    public ALGraph(GraphKind udg, int i, int i1, Object[] vexs, int[][] arcs) {
    }

    @Override
    public void creatGraph() {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入图的类型：");
        GraphKind kind =GraphKind.valueOf(sc.next());
        switch(kind){
            case UDG:
                createUDG();;
                return ;
            case UDN:
                createUDN();
                return;
        }
    }

    private void createUDN() {
        Scanner sc=new Scanner(System.in);
        System.out.println("请分别输入图的顶点数、图的边数：");
        vexNum=sc.nextInt();
        arcNum=sc.nextInt();
        vexs=new  VNode[vexNum];
        System.out.println("请输入图的各个顶点：");
        for(int k=0;k<arcNum;k++){
            int v=locateVex(sc.next());
            int u=locateVex(sc.next());
            int value =sc.nextInt();
            addArc(v,u,value);
            addArc(u,v,value);
        }
    }
    public void addArc(int v,int u,int value){
        ArcNode arc=new ArcNode(u,value);
        arc.nextArc=vexs[v].firstArc;
        vexs[v].firstArc=arc;
    }

    private void createUDG() {
    }

    public int getVexNum() {
        return vexNum;
    }

    @Override
    public int getArcNum() {
        return arcNum;
    }
    public int locateVex(Object vex) {
        for(int v=0;v<vexNum;v++)
            if(vexs[v].data.equals(vex))
                return v;
        return -1;
    }

    public VNode[] getVexs(){
        return vexs;
    }

    public GraphKind getKind(){
        return kind;
    }
    @Override
    public Object getVex(int v) throws Exception {
        if(v<0 && v>vexNum)
            throw new Exception("第"+v+"个顶点不存在！");
        return vexs[v].getData();
    }

    public int firstAdjVex(int v) throws Exception {
        if(v<0 && v>=vexNum)
            throw new Exception("第"+v+"个顶点不存在！");
        VNode vex=vexs[v];
        if(vex.firstArc!=null)
            return vex.firstArc.adjVex;
        return -1;
    }
    public int nextAdjVex(int v,int w)throws Exception{
        if(v<0 && v>=vexNum)
            throw new Exception("第"+v+"个顶点不存在");
        VNode vex=vexs[v];
        ArcNode arcvw=null;
        for(ArcNode arc =vex.firstArc;arc!=null;arc=arc.nextArc)
            if(arc.adjVex==w){
                arcvw=arc;
                break;
            }
        if(arcvw !=null && arcvw.nextArc!=null)
            return arcvw.nextArc.adjVex;
        else
            return -1;
    }

}
